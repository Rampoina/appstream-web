# Description
This is a small tool that generates HTML files from an appstream collection
using mako templates.

# Usage
`python appstream-web.py -p appstream_path -i appstream_icon_path  -m template_path`

# Templates
The template that generates this
[webpage](https://rampoina.gitlab.io/libregameslist/L'Abbaye%20des%20morts.html)
is included in the `templates` directory

# License

This project is licensed under the AGPLv3 LICENSE
