badLicenses = [
    'LicenseRef-proprietary',
    'LicenseRef-Proprietary',
    'proprietary',
    'Proprietary',
    'CC-BY-NC-SA-3.0',
    'CC-BY-NC-ND-3.0'
]

badCategories = [
    'Emulator',
    'PackageManager',
    'System',
    'Utility'
]

nonFreeAssets = [
    'jp.yvt.OpenSpades',
    'net.openra.OpenRA',
    'org.openmw.OpenMW',
    'org.zdoom.GZDoom',
    'io.github.ezQuake',
    'com.etlegacy.ETLegacy',
    'com.github.iortcw.iortcw',
    'org.yamagi.YamagiQ2',
    'org.dhewm3.Dhewm3',
    'com.github.bvschaik.julius',
    'io.openrct2.OpenRCT2',
    'com.github.skullernet.q2pro',
    'org.raceintospace.Raceintospace',
    'org.srb2.SRB2',
    'org.srb2.SRB2Kart',
    'io.sourceforge.clonekeenplus',
    'io.github.fabiangreffrath.Doom',
    'net.dengine.Doomsday',
    'com.github.keriew.augustus',
    'io.github.yairm210.unciv',
    'com.corsixth.corsixth'
]

nonFreeNetworkServices = [
    'io.github.yairm210.unciv'
]

alwaysAccept = [
    'org.freecol.FreeCol',
    'org.freeciv.Freeciv',
    'io.github.EndlessSky.endless-sky',
    'org.frozen_bubble.frozen-bubble',
    'org.kde.ksudoku',
]

alwaysDeny = [
    'com.moonlight_stream.Moonlight',
    'org.gnome.Games',
    'org.ppsspp.PPSSPP',
    'org.scummvm.ScummVM',
    'org.pegasus_frontend.Pegasus',
    'com.gitlab.coringao.cavestory-nx',
    'org.sauerbraten.Sauerbraten',
    'net.runelite.RuneLite',
    'com.zandronum.Zandronum',
    'io.mrarm.mcpelauncher',
    'org.unitystation.StationHub',
    'org.firestormviewer.FirestormViewer',
    'com.eduke32.EDuke32',
    'io.github.hmlendea.geforcenow-electron',
    'io.gdevs.GDLauncher',
    'io.github.sharkwouter.Minigalaxy',
    'com.katawa_shoujo.KatawaShoujo',
    're.chiaki.Chiaki'
]
