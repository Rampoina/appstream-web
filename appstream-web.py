import gi
from gi.repository import Gio
gi.require_version('AppStreamGlib', '1.0')
from gi.repository import AppStreamGlib
from mako.template import Template
from lists import alwaysAccept, alwaysDeny, badLicenses, badCategories,  nonFreeAssets, nonFreeNetworkServices 
import itertools
import argparse

#Workaround to get the urls because app.get_urls() doesn't work :|
def get_urls(app):
    kinds = [AppStreamGlib.UrlKind(kind) for kind in range(11)]
    urls = [(app.get_url_item(kind),kind.value_nick) for kind in kinds]
    return list(filter(lambda a: a[0] is not None, urls))

def acceptedGame(app):
    return 'Game' in app.get_categories()
    '''
    if app.get_id() in alwaysAccept:
        return True
    if app.get_id() in alwaysDeny:
        return False

    return app.get_project_license() and \
        not [x for x in badLicenses if x in app.get_project_license()] and \
        'Game' in app.get_categories() and \
        not [x for x in badCategories if x in app.get_categories()]
    '''

parser = argparse.ArgumentParser(description='Generate HTML files from an Appimage and mako templates.')
parser.add_argument("-p", '--appstream_path', help='specify the path of the appstream file')
parser.add_argument("-i", '--icons_path', help='specify the path of the icons')
parser.add_argument("-m", '--template_path', help='specify the path of the template')
args = parser.parse_args()

store = AppStreamGlib.Store()
file = Gio.File.new_for_path(args.appstream_path)
AppStreamGlib.Store.from_file(store, file, args.icons_path, None)
apps = list(filter(acceptedGame, store.get_apps()))
appTemplate = Template(filename=args.template_path)

for app in apps:
    filename = app.get_name().replace(':', '').replace('/','') + ".html"
    with open(filename,'w') as file:
        print(appTemplate.render(apps=apps,app=app,urls=get_urls(app)), file=file)
